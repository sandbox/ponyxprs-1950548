Spider Control
========
  Spiders login automatically via their IP/useragent/?? the Drupal system gives them a user ID and from there the site owner can assign spider specific permissions - plus other features.


Authors & main contributors
===========================
  Originally by John Rusinko rustyppc@gmail.com


Features
========
*  Identify known, and unknown spiders, crawler, bots, scrapers, etc through hidden link(s)
*  Allow site owner to assign permissions on a per Spider case
*  Spiders can have from full access to no access, site owner specified
*  See what spiders have visited, and when was last visit



*  Comes with a pre-populated list of common spiders (future)


How it works
============
  When a spider visits any Drupal page (or admin-chosen page) that has an invisible .png image on it, that links to another page in the site, the spider will follow the link that will record that spider's ID. Parameters recorded are IP, User agent string, reffering link. From this point the data is used to login this spider in future visits.
  Future features will include a blacklist of pages that disallow spiders, regardless of what the robots.txt file says (rouge bot are known to disregard this file)
  
  
Installation
============

Place this code in a node located at /bait/spider (development purposes for now)
filter type 'php'. Set node for noindex, nofollow, no cache

<?php
  echo('Hello ' . $_SERVER['HTTP_USER_AGENT']);
  echo('Located at ' . $_SERVER['REMOTE_ADDR']);
  spider_control_discover();
?>


  

How to test
===========
To test, browse to www.yoursite.com/bait/spider 
You should then see an entry in the spiders table in your data base with your browser's information recorded.
 

Notes & possible incompatibilities
==================================
